@if (session('msj'))
    <div class="form-group">
        <div class="col-md-12">
            <div class="alert alert-success">
                <label>Mensaje Exitoso</label>
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session('msj') }}
            </div>
        </div>
    </div>
@endif

@if ($errors->any())
    <div class="form-group">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <label>Mensaje de Error</label>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif  