<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Consultamos las tareas del usuario
        $homeworks = User::where('id',Auth::user()->id)->with('homeworks')->first();

        return view('home', ['homework' => $homeworks]);
    }
}
