<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Homework;
use Illuminate\Support\Facades\Auth;

class HomeworkController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('homework.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['user_id'] = Auth::user()->id;

        $validator = $request->validate([
            'name'          =>'required|string',
            'description'   => 'required|string',
            'user_id'       => 'required|integer',
        ]);

        if (!$validator)
        {
            //En el caso de la validacion hayan fallas retornamos el error
            return back()->withErrors($validator)->withInput();
        }
        else{

            //Creamos el nuevo homework
            $homework = Homework::create($request->all());

            //Retornamos el exito de la creacion del homework
            return redirect()->route('home')->with('msj','Homework guardada exitosamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $homework = Homework::find($id);

        if ($homework) 
        {
            if ($homework->user_id == Auth::user()->id) 
            {
                return view('homework.show', ['homework' => $homework]);
            }
            else {

                //Retornamos que no tiene permiso
                return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
            }
        }
        else {

            //Retornamos que no existe el registro
            return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $homework = Homework::find($id);
        
        if ($homework) 
        {
            if ($homework->user_id == Auth::user()->id) 
            {
                return view('homework.edit', ['homework' => $homework]);
            }
            else {

                //Retornamos que no tiene permisos
                return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
            }
        }
        else {

            //Retornamos que existe el registro
            return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $homework = Homework::find($id);
        
        if ($homework) 
        {
            if ($homework->user_id == Auth::user()->id) 
            {
                $homework->update($request->all());

                if ($homework) 
                {
                    //Retornamos el exito de la edición  del homework
                    return redirect()->route('home')->with('msj','Homework editado exitosamente');
                }
                else {

                    //Retornamos que no tiene permisos
                    return redirect()->route('home')->with('msj','Homework no se puedo editar');
                }
            }
            else {

                //Retornamos que no tiene permisos
                return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
            }
        }
        else {

            //Retornamos que no existe el registro
            return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $homework = Homework::find($id);
        
        if ($homework) 
        {
            if ($homework->user_id == Auth::user()->id) 
            {
                $homework->delete();

                if ($homework) 
                {
                    //Retornamos el exito de la eliminación del homework
                    return redirect()->route('home')->with('msj','Homework eliminado exitosamente');
                }
                else {

                    //Retornamos que no tiene permiso
                    return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
                }
            }
            else {

                //Retornamos que no tiene permiso
                return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
            }
        }
        else {

            //Retornamos que no existe el registro
            return redirect()->route('home')->withErrors('Excepción capturada: No puede acceder al Homework')->withInput();
        }
    }
}
