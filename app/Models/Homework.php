<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Homework extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id','created_at', 'updated_at',
    ];


    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
