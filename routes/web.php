<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('homework')->group(function() 
{
    //Crear homework
    Route::get('create','HomeworkController@create')->name('homework.create');
    //Guardar homework
    Route::post('save','HomeworkController@store')->name('homework.store');
    //ver homework
    Route::get('show/{id}','HomeworkController@show')->name('homework.show');
    //Editar homework
    Route::get('edit/{id}','HomeworkController@edit')->name('homework.edit');
    //Actualizar homework
    Route::put('update/{id}','HomeworkController@update')->name('homework.update');
    //Eliminar homework
    Route::delete('destroy/{id}','HomeworkController@destroy')->name('homework.destroy');
});
